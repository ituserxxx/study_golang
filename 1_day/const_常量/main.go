package main

import "fmt"


// 常量
const pai = 3.1415

// 批量声明常量
const (
	n2 = 200
	n3 = 300
	n4 = 400
)
const u, v float32 = 0, 3  // u = 0.0, v = 3.0，常量的多重赋值
const a, b, c = 3, 4, "foo" // a = 3, b = 4, c = "foo", 无类型整型和字符串常量
// 如果某一行没有写值，则后面的默认和前面的一样
const (
	oneHundred = 100
	twoHundred  //100
	threeHundred //100
)

// iota 枚举  在const关键字出现时将被重置为0,每新增一行常量声明则iota值加1
const (
	a1 = iota // 0
	a2 = iota // 1
	_         // 2
	a3        // 3
)

// 插队
const (
	b1 = iota //0
	b2 = 100  //1
	b3 = iota //2
	b4        //3
)

// 多个变量声明在一行
const (
	d1, d2 = iota + 1, iota + 2
	d3, d4 = iota + 1, iota + 2
)

// 定义数量级
const (
	_  = iota
	KB = 1 << (10 * iota)
	MB = 1 << (10 * iota)
	GB = 1 << (10 * iota)
	TB = 1 << (10 * iota)
)
//常量的赋值是一个编译期行为，所以右值不能出现任何需要运行期才能得出结果的表达式，比如以如下方式定义常量就会导致编译错误：

//func GetNumber() int {
//	return 100
//}
//const num = GetNumber()

// 枚举表示法
const (
	Sunday = iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	numberOfDays
)

func main() {
	fmt.Println(twoHundred)
	fmt.Println(a3)
	fmt.Println(b4)
	fmt.Println(d1)
	fmt.Println(d2)
	fmt.Println(d3)
	fmt.Println(d4)
/*
运行以上输出：
	100
	3
	3
	1
	2
	2
	3
 */
}

