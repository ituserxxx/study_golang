package main

import "fmt"

func main() {
	// int
	var a int = 33
	fmt.Printf("%d \n" , a)   //  输出 33  格式化整数
	fmt.Printf("%v \n" , a)   //  输出 33 使用类型的默认输出格式的标识符
	fmt.Printf("%05d \n" , a) // 输出 0033  用于规定输出定长的整数 其中开头的数字 0 是必须的
	fmt.Printf("%v \n", a) // 使用类型的默认输出格式的标识符

	// bool
	var b bool = false
	fmt.Printf("%t \n", b)  //输出 fasle  格式化布尔型
	fmt.Printf("%s \n", b) // 输出 %!s(bool=false)  格式化字符串

	// float64
	var c  = 1.33
	fmt.Printf("%n.5 \n", c) // 用于表示数字 n 并精确到小数点后 m 位，
	fmt.Printf("%f \n", c) // 格式化浮点数
	fmt.Printf("%g \n", c) // 格式化浮点型



	// struct
	type CH3 struct {
		Name string `json:"name"`
	}

	var ch3  =  CH3{Name: "xxx"}
	fmt.Printf("%T \n", ch3) // 打印某个类型的完整说明
	fmt.Printf("%#v \n", ch3) // 打印包括字段和限定类型名称在内的实例的完整信息
	fmt.Printf("%+v \n", ch3) // 打印包括字段在内的实例的完整信息


/*
运行以上输出：
   33
   33
   00033
   33
   false
   %!s(bool=false)
   %!n(float64=1.33).5
   1.330000
   1.33
   main.CH3
   main.CH3{Name:"xxx"}
   {Name:xxx}
*/
}
