package main

import "fmt"

// 指针：一个指针变量值指向一个值的内存地址
// golang 语言取地址符是&
// 放到变量前面就会返回相应变量的内存地址

func main() {
	// 普通变量
	var a int  = 10 //初始化变量并赋值
	fmt.Printf("\na变量地址：[%x]",&a)

	//声明指针变量
	var b *int//指向整型
	var f *float64//指向浮点型

	//指针变量赋值
	var ba int =20
	b = &ba

	var fa float64 = 1.2
	f = &fa

	//指针变量的储存地址
	fmt.Printf("\n通过 b 访问 ba 的值：【%d】",*b)
	fmt.Printf("\n通过 f 访问 fa 的值：【%f】",*f)

	//空指针 值为nil
	var cp *int
	fmt.Printf("\n空指针 cp 的值[%x]",cp) // 输出 0

	//指针判断
	if cp == nil{
		fmt.Printf("cp 是一个空指针")
	}
	/*
	以上输出
	a变量地址：[c0000a8058]
	通过 b 访问 ba 的值：【20】
	通过 f 访问 fa 的值：【1.200000】
	空指针 cp 的值[0]cp 是一个空指针
	 */
}
