package main

import "fmt"

func main() {

	// map 或者 字典
	// 是一个无序集合，key唯一且无序排列， value任意类型
	//非线程安全，并发读写会panic
	//线程安全解决方式：map + sync.RWMutex 或者 sync.Map

	//声明
	var a map[string]int
	// a["ss"] = 1 //此处不能直接赋值，因为只是声明没有初始化

	println(a)
	// 声明且初始化
	var b = map[string]int{
		"one": 1,
		"two": 2,
		"three": 3,
	}
	b["ss"] = 1
	// 使用make初始化
	var c = make(map[string]int,10)
	c["ss"] = 1

	// 查找元素
	//第一个是真正返回的键值，第二个是是否找到的标识，
	//判断是否在字典中成功找到指定的键，不需要检查取到的值是否为 nil，只需查看第二个返回值 ok，这是一个布尔值
	value, ok := b["one"]
	if ok { // 找到了
		// 处理找到的value
		println(value,ok) // 1  true
	}

	//删除元素:
	//Go 语言提供了一个内置函数 delete()，用于删除容器内的元素，
	delete(b,"ss")
	println(b)

	//遍历字典
	for k, v := range b {
		fmt.Println(k, v)
	}

	// 比较 2个map是否相等
	// 都为nil
	// 非空、长度相等，指向同一个map实体对象
	// 相应的key指向的value “深度”相等
}
