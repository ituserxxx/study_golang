package main

import "fmt"

/*
整型是所有编程语言里最基础的数据类型
类型		长度（单位：字节）			说明									值范围								默认值
int8		1					带符号8位整型							-128~127								0
uint8		1					无符号8位整型，与 byte 类型等价			0~255									0
int16		2					带符号16位整型					-32768~32767								0
uint16		2					无符号16位整型						0~65535									0
int32		4					带符号32位整型，与 rune 类型等价	-2147483648~2147483647						0
uint32		4					无符号32位整型						0~4294967295							0
int64		8					带符号64位整型			-9223372036854775808~9223372036854775807			0
uint64		8					无符号64位整型				0~18446744073709551615							0
int		32位或64位				与具体平台相关							与具体平台相关								0
uint	32位或64位				与具体平台相关							与具体平台相关								0
uintptr	与对应指针相同				无符号整型，足以存储指针值的未解释位	32位平台下为4字节，64位平台下为8字节				0


 */
func main() {
	var a int8
	var b uint8
	var c int16
	var d uint16
	var e int32
	fmt.Printf("%T %v \n", a,a)
	fmt.Printf("%T %v \n", b,b)
	fmt.Printf("%T %v \n", c,c)
	fmt.Printf("%T %v \n", d,d)
	fmt.Printf("%T %v \n", e,e)
	/*
	以上输出：
	int8 0
	uint8 0
	int16 0
	uint16 0
	int32 0
	*/
}
