package main

import (
	"fmt"
	"strings"
)

/*
在 Go 语言中，字符串是一种基本类型，默认是通过 UTF-8 编码的字符序列，
当字符为 ASCII 码时则占用 1 个字节，其它字符根据需要占用 2-4 个字节，比如中文编码通常需要 3 个字节。
*/

func main() {
	// 字符串相关
	var s1 string      // 声明字符串变量
	s1 = "Hello World" // 变量初始化

	fmt.Printf("The length of \"%s\" is %d \n", s1, len(s1))

	//对于多行字符串，也可以通过 ` 构建： 原样输出
	s2 := `
		123
			345
		456
	` // 也可以同时进行声明和初始化
	fmt.Printf("%s \n", s2)

	// 字符串拼接：使用 + 连接符
	s3 := "Search s3 for \"Golang\":\n" +
		"- Go\n" +
		"- Golang\n" +
		"- Golang Programming\n"
	fmt.Printf("%s", s3)

	// 字符串占位符：
	fmt.Print(fmt.Sprintf("%s-%s-%d", "hello", "word", 3333))

	// 字符串常用操作：
	var h, w = "hello", "word"
	// 拼接
	var aa = h + " " + w
	// 分割：
	aaArr := strings.Split(aa, " ")
	fmt.Printf("分割: %v", aaArr) // [hello,word]

	// 合并
	bb := strings.Join(aaArr, "")
	fmt.Printf("合并: %v", bb) // hello word

	// 字符串切片：左闭右开规则
	fmt.Printf("获取索引5（不含）之前的子串: %v", bb[:5])
	fmt.Printf("获取索引7（含）之后的子串: %v", bb[7:])
	fmt.Printf("获取从索引0（含）到索引5（不含）之间的子串: %v", bb[0:5])


	//包含  返回的是bool类型
	fmt.Println(strings.Contains(bb, "y"))

	//前缀
	fmt.Println(strings.HasPrefix(bb, "x"))

	//后缀
	fmt.Println(strings.HasSuffix(bb, "x"))

	// 查找下标
	s4 := "abcdef"
	fmt.Println(strings.Index(s4, "c"))
	fmt.Println(strings.LastIndex(s4, "f"))

	n := len(bb)
	for i := 0; i < n; i++ {
		ch := bb[i]    // 依据下标取字符串中的字符
		fmt.Println(i, ch)
	}
}
