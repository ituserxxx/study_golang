package main

import "fmt"

func main() {
	// 算术运算符 + - * / %
	var c int = 3
	var d = 3
	var (
		a = 4
		b = 2
	)

	fmt.Println(a + b) //6
	fmt.Println(a - b) //2
	fmt.Println(a * b) //8
	fmt.Println(a / b) //2
	fmt.Println(a % b) //0
	fmt.Println(c + d) //6
	var e int = 5
	fmt.Println(^e)

	// ++ -- 是单独的语句，不能放在等号的右边
	var s2 = 0
	s2++ // 有效，s2 的值变成 9
	//s2 = s2++ // 无效，编译报错
	//--s2  // 无效，编译报错
	println(s2) // 1

	/*
	位运算符 & | ^ << >>  与 或 异或		【go语言没有 非】
	运算符	含义				结果
	x & y	按位与	把 x 和 y 都为 1 的位设为 1
	x | y	按位或	把 x 或 y 为 1 的位设为 1
	x ^ y	按位异或	把 x 和 y 一个为 1 一个为 0 的位设为 1
	^x		按位取反	把 x 中为 0 的位设为 1，为 1 的位设为 0
	x << y	左移		把 x 中的位向左移动 y 次，每次移动相当于乘以 2
	x >> y	右移		把 x 中的位向右移动 y 次，每次移动相当于除以 2
	*/
	var s1 uint8
	s1 = 255
	s1 = ^s1 // 按位取反
	fmt.Println(s1)   // 0
	s1 = 1
	s1 = s1 << 3 // 左移 3 位，相当于乘以 2^3 = 8
	fmt.Println(s1)       // 8


	// 关系运算符 == != >= <= > <   返回bool类型
	var h,k = 3,3
	if h ==k{
		println("h == k  is:",h==k)
	}else{
		println("h == k  is false")
	}
	/*
	逻辑运算符 && || !	返回bool类型
		运算符	含义							结果
		x && y	逻辑与运算符（AND）	如果 x 和 y 都是 true，则结果为 true，否则结果为 false
		x || y	逻辑或运算符（OR）		如果 x 或 y 是 true，则结果为 true，否则结果为 false
		!x		逻辑非运算符（NOT）	如果 x 为 true，则结果为 false，否则结果为 true
	*/
	if h==4 && k==3{
		println("h && k  is true")
	}else{
		println("h && k  is false")
	}



	//赋值运算符 = <= >= <<= >>= += -= /= %= &= |= ^=
	if h>=4 && k<=3{
		println("h>=4 && k<=3 is true")
	}else{
		println("h>=4 && k<=3 is false")
	}
	/*
	运算符优先级
	上面介绍的 Go 语言运算符优先级如下所示（由上到下表示优先级从高到低，或者数字越大，优先级越高）：
	6      ^（按位取反） !
	5      *  /  %  <<  >>  &  &^
	4      +  -  |  ^（按位异或）
	3      ==  !=  <  <=  >  >=
	2      &&
	1      ||
	 */
	var aa int8 = 10 //只能存8位
	fmt.Println(aa << 10)
}