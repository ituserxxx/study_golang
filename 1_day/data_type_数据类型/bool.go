package main

import "fmt"

/*
Go 支持的数据类型
Go 语言内置对以下这些基本数据类型的支持：
布尔类型：bool
整型：int8、byte、int16、int、uint、uintptr 等
浮点类型：float32、float64
复数类型：complex64、complex128
字符串：string
字符类型：rune
错误类型：error
此外，Go 语言还支持以下这些复合类型：

指针（pointer）
数组（array）
切片（slice）
字典（map）
通道（chan）
结构体（struct）
接口（interface）
*/

func main() {
	// bool
	var v1 bool
	v1 = true
	v2 := (1 == 2) // v2 也会被推导为 bool 类型
	fmt.Printf("v1=%#v, v2=%#v", v1,v2)
	//【Go 是强类型语言，变量类型一旦确定，就不能将其他类型的值赋值给该变量，不同类型不能进行比较】，因此，布尔类型不能接受其他类型的赋值，也不支持自动或强制的类型转换。
	// 以下这些值在进行布尔值判断的时候（使用非严格的 == 比较符）都会被认为是 false
	//布尔值 FALSE 本身
	//整型值 0（零）
	//浮点型值 0.0（零）
	//空字符串，以及字符串 “0”
	//不包括任何元素的数组
	//特殊类型 NULL（包括尚未赋值的变量）
	//从空标记生成的 SimpleXML 对象
	//报错：
	//if false == 0{
	//	println(1)
	//}

	/*
	以上输出：
	v1=true, v2=false
	 */
}