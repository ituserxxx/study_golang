package main

import (
	"fmt"
	"reflect"
)


func main() {

	// 声明单个变量
	var dog string

	// 声明同类型多个
	var cat ,pig string

	// 批量声明多个
	var (
		number  int
		isTrue bool // 推荐小驼峰命名
	)
	// 变量赋值
	dog = "中华田园犬"
	cat = "狸花猫"
	pig = "老母猪"
	number = 18
	isTrue = true

	// Go语言中变量先声明 且 必须使用
	fmt.Printf("\ndog:%s", dog)
	fmt.Printf("\ncat:%s", cat)
	fmt.Printf("\npig:%s", pig)
	fmt.Printf("\nnumber:%d", number)
	fmt.Printf("\nisTrue:%v", isTrue)

	// 变量的初始化+赋值
	var yourname string = "xxx"
	fmt.Printf("\nyourname:%v", yourname)


	// 类型推导
	var bb = 1 //根据值推到出类型
	fmt.Printf("\nbb:%v  类型：%s", bb,reflect.TypeOf(bb))

	// 简短变量声明，只能在函数体内使用
	s3 := "xxx"
	fmt.Printf("\ns3:%v  类型：%s", s3,reflect.TypeOf(bb))
/*
运行以上输出：
   dog:中华田园犬
   cat:狸花猫
   pig:老母猪
   number:18
   isTrue:true
   yourname:xxx
   bb:1  类型：int
   s3:xxx  类型：int
 */
}

