package main

import "fmt"

//全局变量，任何函数中都可使用
var b = 2

// 主函数，入口函数
func main() {
	// 函数调用 get方法
	get()

	//调用 getB()
	getB(b)

	/* 调用函数并返回最大值 */
	fmt.Printf( "\n调用了 max() 方法,最大值是 : %d", max(10, b) )
	// 可变参数
	someArg(1,2,3,5)

	// 函数返回值
	_,k:=swap("sss","kkkk")
	//_ 是Go中的空白标识符。它可以代替任何类型的任何值,表示不接收该参数
	fmt.Printf( "\n调用了 swap() 方法,返回值 : %s", k )

	// 引用传递
	fmt.Printf( "\n调用upB() 方法之前 ,全局变量b值为 : %d", b )
	var upb22=222
	upB(&upb22)
	fmt.Printf( "\n调用upB() 方法之后 ,全局变量b值为 : %d", b )

	/*
	以上输出
	调用了 get() 方法

	调用了 getB() 方法,接收到的参数是：2
	调用了 max() 方法,最大值是 : 10
	调用了 可变参数 someArg() 方法: 1

	调用了 可变参数 someArg() 方法: 2

	调用了 可变参数 someArg() 方法: 3

	调用了 可变参数 someArg() 方法: 5

	调用了 swap() 方法,返回值 : sss
	调用upB() 方法之前 ,全局变量b值为 : 2
	调用upB() 方法之后 ,全局变量b值为 : 222
	Process finished with the exit code 0

	*/
}

// 定义没有参数和返回值的函数方法 名字为get
func get() {
	fmt.Println("调用了 get() 方法")
}
//形式参数：定义函数时，用于接收外部传入的数据，叫做形式参数，简称形参。
//
//实际参数：调用函数时，传给形参的实际的数据，叫做实际参数，简称实参

//实参与形参必须一一对应：顺序，个数，类型

// 定义一个接收1个参数的方法
func getB(p1 int) {
	fmt.Printf("\n调用了 getB() 方法,接收到的参数是：%d", p1)
}

/* 函数返回两个数的最大值 */
func max(num1, num2 int) int {
	/* 定义局部变量 */
	var result int

	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}

//Go函数支持变参。接受变参的函数是有着不定数量的参数的。为了做到这点，首先需要定义函数使其接受变参：
//arg ...int告诉Go这个函数接受不定数量的参数。注意，这些参数的类型全部是int。在函数体中，变量arg是一个int的slice：

func someArg(arg ...int) {
	for _, n := range arg {
		fmt.Printf("\n调用了 可变参数 someArg() 方法: %d\n", n)
	}
}


// 一个函数可以没有返回值，也可以有一个返回值，也可以有返回多个值。
func swap(x, y string) (string, string) {
	return y, x
}


// 引用传递：需要调用的时候使用引用传递
func upB(b2 *int){
	// 这里修改全局变量b的值
	b=*b2
}