package main

import "fmt"

func main() {
	// if 满足条件执行里面的逻辑
	var a int
	if a>0{
		fmt.Printf("\n if a >0 ")
	}else if a== 0{
		fmt.Printf("\n if a =0 ")
	}else{
		fmt.Printf("\n if a <0 ")
	}

	// switch 满足条件执行里面的逻辑
	switch a {
	case 0:
		fmt.Printf("\n switch a =0 ")
	case 1:
		fmt.Printf("\n switch a =1 ")
	}


	switch a {
	case 0:
		fmt.Printf("\n switch2 a =0 ")
		fallthrough //继续往下个case执行
	case 1:
		fmt.Printf("\n switch2 a =1 ")
	}

	// 满足为true则执行里面逻辑
	switch {
	case a ==0:
		fmt.Printf("\n switch3 a =0 ")
		fallthrough //继续往下个case执行
	case a ==2:
		fmt.Printf("\n switch3 a =2 ")
	case func() bool{return true}():
		fmt.Printf("\n switch3 case return false ")
	}
	/*
	以上输出：
	 if a =0
	 switch a =0
	 switch2 a =0
	 switch2 a =1
	 switch3 a =0
	 switch3 a =2
	*/
}
