package main

import "fmt"

// 在func外表示全局
const A =1
var b = 2
func main() {
	// func 内为局部变量
	var c = 1
	fmt.Printf("\n main c is %d", c)

	// 打印全局变量
	fmt.Printf("\nmain fun  b is %d", b)

	// 次数调用 get方法
	get()

	/*
	运行go run scope_of_use_作用域.go 输出
	main c is 1
	main fun  b is 2
	get fun  b is 2
	 */
}

// 定义一个函数方法 名字为get
func get()  {
	//此处不能使用 c变量，c变量在main函数中定义，为局部变量
	//fmt.Printf("get fun  c is %d",c)

	// 打印全局变量
	fmt.Printf("\nget fun  b is %d", b)
}
